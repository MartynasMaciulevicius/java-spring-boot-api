package com.invertisment.app.currency;

import grillbaer.persistentds.PersistentCollections;
import grillbaer.persistentds.PersistentMap;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class ConversionsTest {

    private static class Tuple {
        private final String key;
        private final String value;

        private Tuple(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }


    private ConversionContext tuplesToCtx(Tuple... tuples) {
        final int calculationPrecision = 20;
        PersistentMap<String, BigDecimal> entries = PersistentCollections.persistentBinTreeMap();
        for (Tuple tuple : tuples) {
            entries = entries.put(tuple.key, new BigDecimal(tuple.value));
        }
        return new ConversionContext("EUR", entries, calculationPrecision);
    }

    @Test
    public void testConvertFromBase() {
        BigDecimal output = Conversions.convert(
                tuplesToCtx(new Tuple("USD", "2"), new Tuple("EUR", "99")),
                new BigDecimal("5.03"), "EUR",
                "USD").get();
        assertEquals("2.515", output.stripTrailingZeros().toString());
    }

    @Test
    public void testConvertToBase() {
        BigDecimal output = Conversions.convert(
                tuplesToCtx(new Tuple("USD", "2"), new Tuple("EUR", "99")),
                new BigDecimal("5.02"), "USD",
                "EUR").get();
        assertEquals("10.04", output.stripTrailingZeros().toString());
    }

    @Test
    public void testConvertThroughBase() {
        BigDecimal output = Conversions.convert(
                tuplesToCtx(new Tuple("USD", "5"), new Tuple("EUR", "99"), new Tuple("CHF", "2")),
                new BigDecimal("1"), "USD",
                "CHF").get();
        assertEquals("2.5", output.stripTrailingZeros().toString());
    }

    @Test
    public void testConvertFromBaseToBase() {
        BigDecimal output = Conversions.convert(
                tuplesToCtx(new Tuple("USD", "2"), new Tuple("EUR", "99")),
                new BigDecimal("5.01"), "EUR",
                "EUR").get();
        assertEquals("5.01", output.stripTrailingZeros().toString());
    }

    @Test
    public void testReturnNoneWhenCurrencyNotFound() {
        Optional<BigDecimal> output = Conversions.convert(tuplesToCtx(), new BigDecimal("1"), "USD", "CHF");
        assertFalse(output.isPresent());
    }

    @Test
    public void testFromNonexistent() {
        Optional<BigDecimal> output = Conversions.convert(
                tuplesToCtx(new Tuple("USD", "2")),
                new BigDecimal("1"), "CHF",
                "USD");
        assertFalse(output.isPresent());
    }

    @Test
    public void testToNonexistent() {
        Optional<BigDecimal> output = Conversions.convert(
                tuplesToCtx(new Tuple("USD", "2")),
                new BigDecimal("1"), "USD",
                "CHF");
        assertFalse(output.isPresent());
    }

    @Test
    public void testToIsZero() {
        assertThrows(ArithmeticException.class, () -> {
            Conversions.convert(
                    tuplesToCtx(new Tuple("USD", "5"), new Tuple("EUR", "99"), new Tuple("CHF", "0")),
                    new BigDecimal("1"), "USD",
                    "CHF");
        });
    }

    @Test
    public void testFromIsZero() {
        BigDecimal output = Conversions.convert(
                tuplesToCtx(new Tuple("USD", "0"), new Tuple("EUR", "99"), new Tuple("CHF", "5")),
                new BigDecimal("1"), "USD",
                "CHF").get();
        assertEquals(new BigDecimal(0), output.stripTrailingZeros());
    }

    @Test
    public void testIrrationalNumbers() {
        BigDecimal output = Conversions.convert(
                tuplesToCtx(new Tuple("USD", "1"), new Tuple("EUR", "99"), new Tuple("CHF", "3")),
                new BigDecimal("1"), "EUR",
                "CHF").get();
        assertEquals(new BigDecimal("0.33333333333333333333"), output);
    }

}
