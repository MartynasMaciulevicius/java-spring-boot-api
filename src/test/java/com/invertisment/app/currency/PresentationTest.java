package com.invertisment.app.currency;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class PresentationTest {

    private static final Presentation presentation = new Presentation(18);

    @Test
    public void shouldReturnBasicInput() {
        assertEquals("15", presentation.present(new BigDecimal(15)));
    }

    @Test
    public void shouldRemoveTrailingZeros() {
        assertEquals("15", presentation.present(new BigDecimal("15.000000000")));
    }

    @Test
    public void shouldRoundWhenOverlyPrecise() {
        Presentation presentation = new Presentation(1);
        assertEquals("1.1", presentation.present(new BigDecimal("1.111111111")));
    }

    @Test
    public void shouldRoundWhen18Digits() {
        assertEquals(
                "3.141592653589793238",
                presentation.present(new BigDecimal("3.1415926535897932384626433")));
        assertEquals(
                "3.000000000000000001",
                presentation.present(new BigDecimal("3.0000000000000000005111111")));
        assertEquals(
                "333333333.000000000000000001",
                presentation.present(new BigDecimal("333333333.0000000000000000005111111")));
    }
}
