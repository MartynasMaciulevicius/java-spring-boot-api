package com.invertisment.app.io;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

public class CSVLineReaderTest {

    // get the file url, not working in JAR file.
    private static File getResourceFile(String resourceName) throws IllegalArgumentException {
        URL resource = CSVLineReaderTest.class.getClassLoader().getResource(resourceName);
        if (resource == null) {
            throw new IllegalArgumentException("File not found.");
        }
        try {
            return new File(resource.toURI());
        } catch (URISyntaxException ue) {
            throw new IllegalArgumentException(ue);
        }
    }

    private List<String> loadResourceFile(String resourceName) {
        return CSVLineReader
                .loadCSVFile(getResourceFile(resourceName))
                .collect(Collectors.toList());
    }

    @Test
    public void testLoadFile1() {
        List<String> lines = loadResourceFile("sample-rates-1.csv");
        Assertions.assertFalse(lines.isEmpty());
        Assertions.assertEquals(3, lines.size());
    }

    @Test
    public void testLoadFile2() {
        List<String> lines = loadResourceFile("sample-rates-2.csv");
        Assertions.assertFalse(lines.isEmpty());
        Assertions.assertEquals(4, lines.size());
    }
}
