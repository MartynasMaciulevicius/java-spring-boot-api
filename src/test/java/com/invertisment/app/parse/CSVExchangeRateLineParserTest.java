package com.invertisment.app.parse;

import grillbaer.persistentds.PersistentMap;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class CSVExchangeRateLineParserTest {

    private static final CSVExchangeRateLineParser parser = new CSVExchangeRateLineParser();

    private static PersistentMap<String, BigDecimal> parseCSVLines(String... strings) {
        return parser.parseCSVStream(Stream.of(strings));
    }

    @Test
    public void testParseCSVLines() {
        PersistentMap<String, BigDecimal> entries = parseCSVLines("a,2", "B,3", "c,5", "d,8");
        assertEquals(entries.size(), 4);
        assertEquals(new BigDecimal(2), entries.get("A"));
        assertEquals(new BigDecimal(3), entries.get("B"));
        assertEquals(new BigDecimal(5), entries.get("C"));
        assertEquals(new BigDecimal(8), entries.get("D"));
    }

    @Test
    public void testParseCSVLines2() {
        PersistentMap<String, BigDecimal> entries = parseCSVLines("Eur,15", "usd,81");
        assertEquals(entries.size(), 2);
        assertEquals(new BigDecimal(15), entries.get("EUR"));
        assertEquals(new BigDecimal(81), entries.get("USD"));
    }

    @Test
    public void testShouldCrashOnDuplicateInput() {
        assertThrows(IllegalArgumentException.class, () -> {
            parseCSVLines("Eur,15", "usd,81", "EUR,1");
        });
    }

    @Test
    public void testShouldCrashOnNonNumberInput() {
        assertThrows(IllegalArgumentException.class, () -> {
            parseCSVLines("Eur,15", "usd,hi");
        });
    }

    @Test
    public void testShouldCrashOnInvalidInput() {
        assertThrows(IllegalArgumentException.class, () -> {
            parseCSVLines("15", "usd,1");
        });
    }

    @Test
    public void testParseLongNumbers() {
        PersistentMap<String, BigDecimal> entries = parseCSVLines(
                "Eur,15.111111111111111111", "usd,81.888888888888888881");
        assertEquals(entries.size(), 2);
        assertEquals("15.111111111111111111", entries.get("EUR").toString());
        assertEquals("81.888888888888888881", entries.get("USD").toString());
    }

}
