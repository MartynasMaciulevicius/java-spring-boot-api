package com.invertisment.app.io;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Stream;

public class CSVLineReader {

    public static Stream<String> loadCSVFile(File file) throws IllegalArgumentException {
        try  {
            return new BufferedReader(new FileReader(file)).lines();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
