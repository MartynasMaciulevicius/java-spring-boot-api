package com.invertisment.app.currency;

import grillbaer.persistentds.PersistentMap;

import java.math.BigDecimal;
import java.math.MathContext;

public class ConversionContext {
    public final String baseCurrency;
    public final PersistentMap<String, BigDecimal> ratesToBase;
    public final int calculationPrecision;

    public ConversionContext(String baseCurrency, PersistentMap<String, BigDecimal> ratesToBase, int calculationPrecision) {
        this.baseCurrency = baseCurrency;
        this.ratesToBase = ratesToBase;
        this.calculationPrecision = calculationPrecision;
    }
}
