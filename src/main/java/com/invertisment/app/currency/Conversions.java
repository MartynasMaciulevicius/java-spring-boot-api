package com.invertisment.app.currency;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;
import java.util.function.BiFunction;

public class Conversions {

    private static Optional<BigDecimal> convertGeneric(ConversionContext context,
                                                String currency,
                                                BiFunction<BigDecimal, BigDecimal, BigDecimal> convertFn,
                                                BigDecimal amount) {
        if (currency.equals(context.baseCurrency)) {
            return Optional.of(amount);
        }
        BigDecimal fromRate = context.ratesToBase.get(currency);
        if (fromRate == null) {
            return Optional.empty();
        }
        return Optional.of(convertFn.apply(amount, fromRate));
    }

    private static Optional<BigDecimal> convertToBase(ConversionContext context, String from, BigDecimal amount) {
        return convertGeneric(
                context,
                from,
                BigDecimal::multiply,
                amount);
    }

    private static Optional<BigDecimal> convertFromBase(ConversionContext context, String to, BigDecimal amount) {
        return convertGeneric(
                context,
                to,
                (baseAmount, baseMultiplier) -> baseAmount.divide(baseMultiplier, RoundingMode.HALF_UP),
                amount);
    }

    public static Optional<BigDecimal> convert(ConversionContext context, BigDecimal amount, String from, String to) {
        if (from.equals(to)) {
            return Optional.of(amount);
        }
        return convertToBase(context, from, amount.setScale(context.calculationPrecision, RoundingMode.HALF_UP))
                .flatMap(a -> convertFromBase(context, to, a));
    }
}
