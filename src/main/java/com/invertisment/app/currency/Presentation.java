package com.invertisment.app.currency;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class Presentation {

    private final int precision;

    public Presentation(int precision) {
        this.precision = precision;
    }

    public String present(BigDecimal amount) {
        return amount
                .setScale(this.precision, RoundingMode.HALF_UP)
                .stripTrailingZeros()
                .toString();
    }
}
