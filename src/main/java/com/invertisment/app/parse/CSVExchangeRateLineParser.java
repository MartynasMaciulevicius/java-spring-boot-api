package com.invertisment.app.parse;

import grillbaer.persistentds.PersistentCollections;
import grillbaer.persistentds.PersistentMap;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.stream.Stream;

public class CSVExchangeRateLineParser {

    public static PersistentMap<String, BigDecimal> parseCSVStream(Stream<String> lines) throws IllegalArgumentException {
        try (lines) {
            PersistentMap<String, BigDecimal> entries = PersistentCollections.persistentBinTreeMap();
            for (Iterator<String> it = lines.iterator(); it.hasNext(); ) {
                String line = it.next();
                String[] split = line.split(",");
                if (split.length < 2) {
                    throw new IllegalArgumentException("Not enough data items in line: " + line);
                }
                String key = split[0].toUpperCase();
                if (entries.containsKey(key)) {
                    throw new IllegalArgumentException("Duplicate key: " + key);
                }
                entries = entries.put(key, new BigDecimal(split[1]));
            }
            return entries;
        }
    }
}
