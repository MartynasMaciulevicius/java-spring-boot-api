package com.invertisment.app;

import com.invertisment.app.currency.ConversionContext;
import com.invertisment.app.currency.Conversions;
import com.invertisment.app.currency.Presentation;
import com.invertisment.app.io.CSVLineReader;
import com.invertisment.app.parse.CSVExchangeRateLineParser;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Optional;

// Guide: https://spring.io/quickstart
// Boot+Swagger: https://github.com/ityouknow/spring-boot-examples/blob/master/spring-boot-swagger/pom.xml
@SpringBootApplication
@RestController
public class DemoApplication {

	public final int ROUNDING_PRECISION = 18;
	public final int CALCULATION_PRECISION = ROUNDING_PRECISION + 2;

	private final Presentation presentation = new Presentation(ROUNDING_PRECISION);
	private ConversionContext conversionContext;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@PostConstruct
	public void init() {
		reloadDataFile();
	}

	private String htmlLink(String href) {
	    return String.format("<a href=\"%s\">%s</a>", href, href);
	}

	@GetMapping("/")
	public String mainPage() {
		return "Possible requests:<br>" +
				"GET " + htmlLink("/reload-exchange-rates") + "<br>" +
				"GET " + htmlLink("/exchange?from=BTC&to=EUR&amount=1") + "<br>";
	}

	public void reloadDataFile() {
		this.conversionContext = new ConversionContext(
				"EUR",
				CSVExchangeRateLineParser.parseCSVStream(
						CSVLineReader.loadCSVFile(new File("exchange-rates.csv"))),
				CALCULATION_PRECISION);
	}

	@GetMapping("/reload-exchange-rates")
	public String reloadDataFileReq() {
		this.reloadDataFile();
		return "OK";
	}

	@GetMapping("/exchange")
	public ResponseEntity<String> convert(@RequestParam(value = "from", defaultValue = "EUR") String from,
										  @RequestParam(value = "to", defaultValue = "EUR") String to,
										  @RequestParam(value = "amount", defaultValue = "0") String amount
	) {
		try {
			BigDecimal parsedAmount = new BigDecimal(amount);
			Optional<String> converted = Conversions.convert(this.conversionContext, parsedAmount, from, to)
					.map(presentation::present);
			return ResponseEntity.of(converted);
		} catch (NumberFormatException e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

}
