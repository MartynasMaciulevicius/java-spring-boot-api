## Java Spring Boot app

Parašyti aplikaciją (gali nebūti UI), kurį išpildytų reikalavimus:
1. Turėtų REST API
2. Naudoja duotą csv su valiutų keitimo kursais (name, exchangeRate <kursai nurodyti į EUR>)
3. Pagal naudotojo įvestą informaciją (kiekis, pradinė valiuta, galutinė valiuta) išveda kiekį į nurodytą
valiutą.
4. Išvedamas kiekis turi būti tikslus iki 18 skaičių po kablelio
5. Turi automatinius testus, kurie parodo programos veiksmingumą
6. Turėtų būti parašyta JAVA naudojant SpringBoot.

### Test run instructions

`gradle test`

### Prod run instructions
// Source: https://www.baeldung.com/spring-boot-gradle-plugin

```
gradle build
java -jar build/libs/app-0.0.1-SNAPSHOT.jar
```

### Basic run instructions
`gradle bootRun`
